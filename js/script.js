//example of functions calling functions

var work = function(){
	console.log("working!");
};

var doWork = function(f){

	console.log("starting");

	try{
		f();
	}
	catch(ex){
		console.log(ex);
	}
	console.log("end")
}


doWork(work);

//example of abstractions

var createWorker = function(){
	var workCount = 0;

	var task1 = function(){
		workCount += 1;
		console.log("task1 "+ workCount)
	};
	var task2 = function(){
		workCount += 1;
		console.log("task2 "+ workCount)
	};

	return{
		job1: task1,
		job2: task2
	};

};

var worker = createWorker();

worker.job1();
worker.job2();
worker.job1();
worker.job2();
worker.job1();

// first usage of angular..
//The data created by the model - generating the hello message
//is successfully being passed to the HTML! Using AngularJS

//This first step created a module... and all controllers reside within the module
var app = angular.module('myApp', []);

//first controller
app.controller('MainController', function($scope){
	$scope.message = "Hello Peter";
});

//Second example, this separates the definition of the controller from the execution, it also works.	
var PersonController = function($scope){
	
	var person = {
		firstName: "Cowboy",
		lastName: "Bebop",
		imageSrc: "images/cbposter.jpg"
	};

	$scope.person = person;
}

app.controller("PersonController", PersonController);

//third example
//a model that retrieves data from a server... using AngularJS

var HTTPController = function($scope, $http){

	var onUserComplete = function(response){
		$scope.user = response.data;
	};
    
    //the http doesn't return data right away, that is why
    //we have to use the then command... only then can we
    //have the data for scope.user
	$http.get("https://api.github.com/users/robconery").then(onUserComplete);
};

app.controller("HTTPController", HTTPController);





