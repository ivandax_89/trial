//This is working!!!!
(function(){

	var app = angular.module('myApp', []);

	//HTTP controller to retrieve data from a given web repository
	var HTTPController = function($scope, github, $interval, $log, $location, $anchorScroll){

		var onUserComplete = function(data){
			$scope.user = data;
			//if we want to also go inside one of the urls provide to get the repository...
			github.getRepos($scope.user).then(onRepos, onError);
		};

        //this allows me to access the new list of objects returned from the repos url we got...
		var onRepos = function(data){
			$scope.repos = data;
			//we do it here cause now we know we retrieved everything
			$location.hash("userdetails");
			$anchorScroll();
		};

		var onError = function(reason){
			$scope.error = "Could not fetch the data.";
		};

		//a function to play the countdown
		//when countdown reaches 0 the search will be performed with whatever is set before user input...
		var decrementCountdown = function(){
			$scope.countdown -= 1;
			if($scope.countdown < 1){
				$scope.search($scope.username);
			};
		};

        var countdownInterval = null;
		var startCountdown = function(){
			countdownInterval = $interval(decrementCountdown, 1000, $scope.countdown)
		};

		$scope.search = function(username){
			$log.info("Searching for "+username);
			github.getUser(username).then(onUserComplete, onError);
			if(countdownInterval){
				$interval.cancel(countdownInterval);
				$scope.countdown = null;
			}
		};
	    
	    //default value to appear
	    $scope.username = "angular";
	    //
	    $scope.repoSortOrder = "+id";

	    $scope.countdown = 5;

	    startCountdown();
	    
		// $http.get("https://api.github.com/users/robconery").then(onUserComplete, onError);

	};

	app.controller("HTTPController", ["$scope", "github", "$interval", "$log", "$location", "$anchorScroll", HTTPController]);

}());





