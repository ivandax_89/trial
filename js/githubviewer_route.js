//This is working!!!!
(function(){

	var app = angular.module('myApp');

	//HTTP controller to retrieve data from a given web repository
	var HTTPController = function($scope, $interval,$location){

		//a function to play the countdown
		//when countdown reaches 0 the search will be performed with whatever is set before user input...
		var decrementCountdown = function(){
			$scope.countdown -= 1;
			if($scope.countdown < 1){
				$scope.search($scope.username);
			};
		};

        var countdownInterval = null;
		var startCountdown = function(){
			countdownInterval = $interval(decrementCountdown, 1000, $scope.countdown)
		};

		$scope.search = function(username){
			if(countdownInterval){
				$interval.cancel(countdownInterval);
				$scope.countdown = null;
			}
		};
	    
	    //default value to appear
	    $scope.username = "angular";

	    $scope.countdown = 5;

	    startCountdown();

	};

	app.controller("HTTPController", ["$scope", "$interval", "$location", HTTPController]);

}());





