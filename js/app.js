(function(){

	var app = angular.module('myApp', ["ngRoute"]);

	app.config(function($routeProvider){
		$routeProvider
		.when("/main", {
			templateUrl: "https://www.belatedvalentine.com/course/githubviewer/main.html",
			controller: "HTTPController"
		}).otherwise({redirectTo:"/main"});
	});

}());